<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="robots" content="index, nofollow" />
    <meta name="author" content="Королёв Николай" />
    <meta name="copyright" lang="ru" content="DevEducation" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Online wallet, crypto wallet , wallet , bitcoin wallet">
    <meta name="description" content="Cryptocurrency exchange - We operate the worlds biggest bitcoin exchange and altcoin crypto exchange in the world by volume">
    <link rel="stylesheet" href="../css/style.css">
    <title>Online wallet authorization page </title>
</head>
<body>
<div class="dashboard-page">
    <header class="dashboard-page__header header-dashboard">
        <div class="header-dashboard__logo logo">
            <img src="../img/logo.svg"  class="logo__img" alt="Logo Online Wallet">
            <h1 class="logo__h1"></h1>
        </div>
        <div class="header-dashboard__account account-box">
            <img src="../img/userpic.jpg" alt="Изображение не доступно" class="account-box__img">
            <div class="account-box__user-name">
                <?php
                  $link = mysqli_connect("localhost", "h32081c_test", "test1test12", "h32081c_test");
                   if (!$link) {
                      echo 'Не могу соединиться с БД. Код ошибки: ' . mysqli_connect_errno() . ', ошибка: ' . mysqli_connect_error();
                      exit;
                    }
                    session_start();
                    if (isset($_SESSION['username']))
                    echo  $_SESSION['set'];
                    else
                    echo  $_SESSION['unset'];
                ?>
            </div>
            <img src="../img/arrow-open.svg" alt="" class="account-box__arrow account-box__arrow--rotate">
        </div>
    </header>
    <div class="dashboard-page__content content-container">
       <div class="content-container__img">
         <h1 style="font-size:40px;">Мы работаем над функционалом этой страницы</h1>
           <img src="../img/development.gif" alt="intro">
       </div>
   </div>
</div>
<script src="../js/init.js"></script>
<script src="../js/model.js" ></script>
<script src="../js/controller.js" ></script>
<script src="../js/logic.js" ></script>


</body>
</html>