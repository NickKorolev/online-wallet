<?php
    
    $authcode = isset($_POST['authcode']) ? $_POST['authcode'] : '';
    $ok = true;
    $messages = array();

    if ($ok) {
        if ($authcode === '123456') {
            $ok = true;
            $messages[] = 'Successful login!';
        } else {
            $ok = false;
            $messages[] = 'Incorrect code!';
        }
    }
    echo json_encode(
        array(
            'ok' => $ok,
            'messages' => $messages
        )
    );
?>