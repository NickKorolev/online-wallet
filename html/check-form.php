<?php
    $login = isset($_POST['login']) ? $_POST['login'] : '';
    $password = isset($_POST['password']) ? $_POST['password'] : '';
    $device = isset($_POST['device']) ? $_POST['device'] : '';
    $ok = true;
    $messages = array();
    if ( !isset($login) || empty($login) ) {
        $ok = false;
        $messages[] = 'login cannot be empty!';
    }
    if ( !isset($password) || empty($password) ) {
        $ok = false;
        $messages[] = 'Password cannot be empty!';
    }
     if ( !isset($device) || empty($device) ) {
            $ok = false;
            $messages[] = 'Device cannot be empty!';
        }
    if ($ok) {
        if ($login === 'tester' && $password === '123456' && $device === 'tester') {
            $ok = true;
            $messages[] = 'Successful login!';
        } else {
            $ok = false;
            $messages[] = 'Incorrect login/password combination!';
        }
    }
    //header('Content-Type: application/json; charset=UTF-8');
    echo json_encode(
        array(
            'ok' => $ok,
            'messages' => $messages
        )
    );
?>