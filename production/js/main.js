const toggleViewPasswordBth = document.querySelectorAll(".form-login__switch-password")[0];
const dropDownLangActive = document.querySelector('.dropdown__lang');
const inputPassword = document.getElementById('input-password');
const closeModal = document.querySelectorAll('.modal__close')[0];
const closeModalIos = document.getElementById('close-ios');
const overlay = document.querySelectorAll('.overlay')[0];
const dropdown = document.getElementById('langActive');
const modalIos = document.getElementById('modal-ios');
const qrCode = document.getElementById('qr-android');
const menuLang = document.querySelector('.menu-lang');
const qrCodeIos = document.getElementById('qr-ios');
const modal = document.querySelectorAll('.modal')[0];

dropdown.addEventListener('click', function(){
    menuLang.classList.toggle("active");
    dropDownLangActive.classList.toggle("active");

});

qrCode.addEventListener('click', function(){
    modal.classList.add("show");
    overlay.classList.add("show");
});

qrCodeIos.addEventListener('click', function(){
    modalIos.classList.add("show");
    overlay.classList.add("show");
});

overlay.addEventListener('click', function(){
    modal.classList.remove("show");
    overlay.classList.remove("show");
    modalIos.classList.remove("show");

});

closeModal.addEventListener('click', function(){
    modal.classList.remove("show");
    overlay.classList.remove("show");
});

closeModalIos.addEventListener('click', function(){
    modalIos.classList.remove("show");
    overlay.classList.remove("show");
});

function toggleShowPassword() {
    if (inputPassword.type === "password") {
        inputPassword.type = "text";
    } else {
        inputPassword.type = "password";
    }
};

toggleViewPasswordBth.addEventListener('click' , function (){
    toggleShowPassword();
});



