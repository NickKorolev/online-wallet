const form = {
    login: document.getElementById('login'),
    password: document.getElementById('input-password'),
    device: document.getElementById('device'),
    submit: document.getElementById('submit'),
    errorMessage: document.querySelector('.form-login__error')
};
form.submit.addEventListener('click', () => {
    const request = new XMLHttpRequest();
    request.onload = () => {
        let responseObject = null;
        try {
            responseObject = JSON.parse(request.responseText);
        } catch (e) {
            console.error('Could not parse JSON!');
        }
        if (responseObject) {
            handleResponse(responseObject);
        }
    };
    const requestData = `login=${form.login.value}&device=${form.device.value}&password=${form.password.value}`;
    request.open('post', 'check-login.php');
    request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    request.send(requestData);
});


function handleResponse (responseObject) {
    if (responseObject.ok) {
        location.href = 'development/development.php';
    } else {
        form.errorMessage.style.display = "flex";
    }
}