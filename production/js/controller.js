function initElements() {
    toggleViewPasswordBth = document.querySelector(".form-row__switch-password");
    dropDownLangActive = document.querySelector('.header-dropdown__lang ');
    inputPassword = document.getElementById('input-password');
    closeModal = document.querySelectorAll('.modal__close')[0];
    qrCodeAndroid = document.getElementById('qr-android');
    closeModalIos = document.getElementById('close-ios');
    dropdown = document.getElementById('langActive');
    modalIos = document.getElementById('modal-ios');
    menuLang = document.querySelector('.menu-lang');
    qrCodeIos = document.getElementById('qr-ios');
    modal = document.querySelectorAll('.modal')[0];
    overlay = document.querySelector('.overlay');
    allLanguageButton = document.querySelectorAll(".menu-lang__nav");
    langActive = document.querySelector(".dropdown-btn__lang");
};


function addListeners() {
    dropdown.tabindex = "0";

    dropdown.addEventListener('click', function (e) {
        menuLang.classList.toggle("active");
        dropDownLangActive.classList.toggle("active");

        chooseLanguage();
    });

    dropdown.addEventListener('focusout', function(e){
        window.setTimeout(function()
        {
            menuLang.classList.remove("active");
            dropDownLangActive.classList.remove("active");
        }, 100);
    });

    qrCodeAndroid.addEventListener('click', function(){
        modal.classList.add("show");
        overlay.classList.add("show");
    });

    qrCodeIos.addEventListener('click', function(){
        overlay.classList.add("show");
        modalIos.classList.add("show");
    });

    overlay.addEventListener('click', function(){
        modal.classList.remove("show");
        overlay.classList.remove("show");
        modalIos.classList.remove("show");

    });


    closeModal.addEventListener('click', function(){
        modal.classList.remove("show");
        overlay.classList.remove("show");
    });

    closeModalIos.addEventListener('click', function(){
        overlay.classList.remove("show");
        modalIos.classList.remove("show");
    });
    toggleViewPasswordBth.addEventListener('click' , function (){
        toggleShowPassword()
    });
};

function toggleShowPassword() {
    if (inputPassword.type === "password") {
        inputPassword.type = "text";
    } else {
        inputPassword.type = "password";
    }
};

