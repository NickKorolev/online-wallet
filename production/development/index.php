<?php
    session_start();
    $servername = '127.0.0.1';
    $username = 'username';
    $password = 'password';
    $dbname = 'Users';
    $conn = new mysqli($servername, $username, $password, $dbname);
    $username = '';
    if (isset($_SESSION["user"])) $username = $_SESSION["user"];
    if (isset($_POST['user'])) $username = $_POST['user'];

    $password = '';
    if (isset($_SESSION["pass"])) $password = $_SESSION["pass"];
    if (isset($_POST['pass'])) $password = $_POST['pass'];
    $error = 0;
    if (isset($_GET['error'])) $error = $_GET['error'];
    $gotoparam = 'index.php';
    $sqlSearchUser = "SELECT * FROM `Users` WHERE User LIKE '$username'";
    $result = $conn->query($sqlSearchUser);
    $row = $result->fetch_assoc();
        
    if ($username == $row["User"] && $password == $row["Password"] && $username != '' && $password != '') {
        $_SESSION["user"] = "$username";
        $_SESSION["pass"] = "$password";
        header("Location: /inventory.php?database=LastUpdate");
    } elseif ($username != '' && $password != '') {
        $gotoparam = '/index.php?error=1';
    };
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="robots" content="index, nofollow" />
    <meta name="author" content="Королёв Николай" />
    <meta name="copyright" lang="ru" content="DevEducation" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Online wallet, crypto wallet , wallet , bitcoin wallet">
    <meta name="description" content="Cryptocurrency exchange - We operate the worlds biggest bitcoin exchange and altcoin crypto exchange in the world by volume">
    <link rel="stylesheet/less" href="../less/main.less">
    <script src="../js/less.min.js" ></script>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <title>Online wallet authorization page </title>
</head>
<body>
<div class="login-page">1111111111111111111111
    <header class="login-page__header header-dropdown">
        <div class=" header-dropdown__lang  dropdown-btn" id="langActive">
            <div class="dropdown-btn__lang">English</div>
            <span class="dropdown-btn__arrow"></span>
        </div>
        <nav class=" header-dropdown__menu menu-lang">
            <div class="menu-lang__nav">Deutsch - German</div>
            <div class="menu-lang__nav">polski - Polish</div>
            <div class="menu-lang__nav menu-lang__nav--active">English</div>
            <div class="menu-lang__nav">русский - Russian</div>
            <div class="menu-lang__nav">ةيبرعلا - Arabic</div>
            <div class="menu-lang__nav">español - Spanish</div>
            <div class="menu-lang__nav">italiano - Italian</div>
        </nav>
    </header>
    <div class="login-page__content content">
        <div class="content__description description-login">
            <div class="description-login__logo logo">
                <img src="../img/logo.svg"  class="logo__img" alt="Logo Online Wallet">
                <h1 class="logo__h1"></h1>
            </div>
            <div class="description-login__description">
                For login you have to download the mobile app and be registered in.
            </div>
            <div class="description-login__info">If you have already registered please enter your data and click Log in button.</div>
            <div class="description-login__downloads download-app">
                <div class="download-app__box download-box">
                    <div class="download-box__img">
                        <a href="#">
                            <img src="../img/GooglePlay.svg" alt="Android market">
                        </a>
                    </div>
                    <div class="download-box__qrcode" id="qr-android">Show QR-code</div>
                </div>
                <div class="download-app__box download-box">
                    <div class="download-box__img">
                        <a href="#">
                             <img src="../img/app-store.svg" alt="Android market">
                        </a>
                    </div>
                    <div class="download-box__qrcode" id="qr-ios">Show QR-code</div>
                </div>
            </div>
        </div>



        <div class="content__form form-login">
        <?php
            if ($error == 1) echo '<div class="error">Invalid username or password</div>';
        ?>
            <div class="form-login__error">
                <div class="form-login__ico-error"></div>
                <div class="form-login__txt">The login, device name or password is not correct.</div>
            </div>

            <form action="<?php echo $gotoparam ?>" method="post" class="form-login__form form">
                <div class="form__head">Log In</div>
                <div class="form__row form-row">
                    <div class="form-row__label">Login</div>
                    <input type="text" name="login" pattern="[A-Za-z0-9]{4,}" maxlength="20" class="form-row__input" required />
                </div>
                <div class="form__row form-row">
                    <div class="form-row__label">Device Name</div>
                    <input type="text" name="device" pattern="[A-Za-z0-9]{6,}" maxlength="20" class="form-row__input" required />
                </div>
                <div class="form__row form-row">
                    <div class="form-row__label">Password</div>
                    <input type="password" name="password" pattern="[A-Za-z0-9]{6,}"  maxlength="20" class="form-row__input" id="input-password" required />
                    <span class="form-row__switch-password"></span>
                </div>
                <div class="form__row form-row">
                    <button type="submit" name="submit" class="form-row__input form-row__input--submit">log in </button>
                </div>
            </form>
        </div>



    </div>
    <div class="login-page__bottom-bg"></div>

    <div class="overlay" id="overlay"></div>
    <div class="modal" id="modal">
        <div class="modal__close"></div>
        <div class="modal__img">
            <img src="../img/qr.svg" alt="modal android">
        </div>
        <div class="modal__txt">Scan the QR code to go to Play Market</div>
    </div>
    <div class="modal" id="modal-ios">
        <div class="modal__close" id="close-ios"></div>
        <div class="modal__img">
            <img src="../img/qr-apple.svg" alt="modal android">
        </div>
        <div class="modal__txt">Scan the QR code to go to App Store</div>
    </div>
</div>

<script src="../js/init.js"></script>
<script src="../js/model.js" ></script>
<script src="../js/controller.js" ></script>
<script src="../js/dropdawnmenu.js" ></script>
<!--script src="../js/logic.js" ></script-->


</body>
</html>