function initElements() {
    toggleViewPasswordBth = document.querySelector(".form-row__switch-password");
    overlayVerification = document.getElementById('overlayVerification');
    submitVerification = document.querySelector('.modal-verification__submit');
    dropDownLangActive = document.querySelector('.header-dropdown__lang ');
    modalVerification = document.getElementById('modalVerification');
    allLanguageButton = document.querySelectorAll(".menu-lang__nav");
    formErrorMessage = document.querySelector('.form-login__error');
    mobileStubClose = document.querySelector('.mobile-stub__close');
    modalErrorClose = document.querySelector('.modal-error__close');
    inputPassword = document.getElementById('input-password');
    qrCodeAndroid = document.getElementById('qrAndroid');
    formLoginJoke = document.querySelector('.form-login');
    closeModalIos = document.getElementById('close-ios');
    formPassword = document.getElementById('input-password');
    closeModal = document.querySelectorAll('.modal__close')[0];
    langActive = document.querySelector(".dropdown-btn__lang");
    modalError = document.getElementById('modalError');
    mobileStub = document.querySelector('.mobile-stub');
    formDevice = document.getElementById('device');
    formSubmit = document.getElementById('submit');
    qrCodeIos = document.getElementById('qrIos');
    formLogin = document.getElementById('login');
    dropdown = document.getElementById('langActive');
    modalIos = document.getElementById('modalIos');
    menuLang = document.querySelector('.menu-lang');
    overlay = document.querySelector('.overlay');
    modal = document.querySelectorAll('.modal')[0];
};

function addListeners() {
    dropdown.tabindex = "0";
    dropdown.addEventListener('click', function (e) {
        menuLang.classList.toggle("active");
        dropDownLangActive.classList.toggle("active");
        chooseLanguage();
    });

    dropdown.addEventListener('focusout', function(e){
        window.setTimeout(function()
        {
            menuLang.classList.remove("active");
            dropDownLangActive.classList.remove("active");
        }, 100);
    });

    qrCodeAndroid.addEventListener('click', function(){
        modal.classList.add("show");
        overlay.classList.add("show");
    });

    qrCodeIos.addEventListener('click', function(){
        overlay.classList.add("show");
        modalIos.classList.add("show");
    });

    overlay.addEventListener('click', function(){
        modal.classList.remove("show");
        overlay.classList.remove("show");
        modalIos.classList.remove("show");
    });

    closeModal.addEventListener('click', function(){
        modal.classList.remove("show");
        overlay.classList.remove("show");
    });

    closeModalIos.addEventListener('click', function(){
        overlay.classList.remove("show");
        modalIos.classList.remove("show");
    });

    toggleViewPasswordBth.addEventListener('click' , function (){
        toggleShowPassword()
    });

    modalErrorClose.addEventListener('click' , function () {
        overlayVerification.classList.remove('show');
        modalError.classList.remove('show');
    });

    mobileStubClose.addEventListener('click' , function () {
        mobileStub.classList.add('hidden')
    });

    window.addEventListener("keydown", function(e) {
        if (e.ctrlKey && e.altKey)
            formLoginJoke.classList.add('joke');
    });

    window.addEventListener("keydown", function(e) {
        if (e.ctrlKey && e.altKey && e.keyCode == 82)
            formLoginJoke.classList.remove('joke');
    });
};

function toggleShowPassword() {
    if (inputPassword.type === "password") {
        inputPassword.type = "text";
    } else {
        inputPassword.type = "password";
    }
};



