formTwoFactor.submitVerification.addEventListener('click', function(e) {
    e.preventDefault();
    const requestAuth = new XMLHttpRequest();
    requestAuth.onload = () => {
        let responseObject = null;
        try {
            responseObject = JSON.parse(requestAuth.responseText);
        } catch (e) {
            console.error('Could not parse JSON!');
        }
        if (responseObject) {
            handleResponseAuth(responseObject);
        }
    };
    const requestDataAuth = `authcode=${formTwoFactor.twoFactorAuthentification.value}`;
    requestAuth.open('post', 'check-twofactor.php');
    requestAuth.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    requestAuth.send(requestDataAuth);
});

function handleResponseAuth (responseObject) {
    if (responseObject.ok) {
        location.href = 'development/development.php';
    } else {
        modalVerification.classList.remove('show');
        modalError.classList.add('show');
    }
};