function chooseLanguage() {
    for (let i = 0; i < allLanguageButton.length; i++){
        allLanguageButton[i].addEventListener("click", function (e) {
            let langMenuBtns = allLanguageButton[0];
            let stringSlice = '';
            stringSlice += `${e.target.textContent}`;
            if(!stringSlice.includes('-')){
                langActive.innerHTML = `${e.target.textContent}`;
            } else{
                let stringSLiceLang = stringSlice.lastIndexOf('-');
                let stringOnActiveLanguage = stringSlice.substr(0 , stringSLiceLang );
                langActive.innerHTML = stringOnActiveLanguage;
            }

            while(langMenuBtns){
                if(langMenuBtns.tagName === "DIV"){
                    langMenuBtns.classList.remove('menu-lang__nav--active');
                }
                langMenuBtns = langMenuBtns.nextSibling;
            };
            this.classList.add('menu-lang__nav--active');
            dropdown.classList.remove("active");
            menuLang.classList.remove("active");
        });
    };
};










