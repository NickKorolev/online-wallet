document.addEventListener("DOMContentLoaded", function () {
    init();
});

function init() {
    initElements();
    addListeners();
}